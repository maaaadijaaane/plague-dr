﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

[RequireComponent(typeof(Slider))]
public class SliderController : MonoBehaviour
{
    private float currValue;
    Slider Slider{
        get{return GetComponent<Slider>();}
    }

    void Start()
    {
        currValue = Slider.value;
    }

    public AudioMixer mixer;
    public string volumeName;

    public void UpdateValueOnChange()
    {
        Debug.Log("Updating volume: " + Slider.value);
        mixer.SetFloat(volumeName, Slider.value);
        currValue = Slider.value;
    }
}
