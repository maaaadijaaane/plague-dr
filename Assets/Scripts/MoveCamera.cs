﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour
{
    Camera mCamera;
    public bool CamMove;
    public bool CamRevert;

    // Start is called before the first frame update
    void Start()
    {
        mCamera = Camera.main;
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            CameraControl controlEnable = mCamera.GetComponent<CameraControl>();

            if(CamMove)
            {
                controlEnable.CameraMove();
            } else if (CamRevert)
            {
                controlEnable.CameraRevert();
            }
            
            
        }
    }

}
