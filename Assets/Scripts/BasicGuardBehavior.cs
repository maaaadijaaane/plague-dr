﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicGuardBehavior : MonoBehaviour
{
    private Animator guardAnim;

    // Start is called before the first frame update
    void Start()
    {
        //isDeath = false;
        guardAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            print("Player attack");
            Death();
        }
    }

    private void Death()
    {
        guardAnim.SetBool("playerAttack", true);

    }

    private void AfterDeath()
    {
        guardAnim.enabled = false;

    }
}
