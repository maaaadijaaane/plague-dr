﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveset : MonoBehaviour
{
    public AudioClip moveSound;
    public AudioClip eliminateSound;
    public AudioClip deathSound;
    public AudioClip levelComplete;
    public float moveSpeed = 10f;
    public float rayDistance = 10f;
    private Ray rayForward;
    private Ray rayBackward;
    private Ray rayLeft;
    private Ray rayRight;
    private RaycastHit hit;
    private CapsuleCollider playerCollider;
    private GameObject playerDone;
    private GameObject playerDeath;
    // Initialize an array for all enemies in scene
    private GameObject[] enemies;

    // Advanced Enemy Movement
    // For Testing
    // To use set in inspector the point target and uncomment on update button jump. Then press space during play. Will set all advanced movement to true and set the target to that space.
    // public Transform newPoint;

    // Animator
    private Animator playerAnim;

    // NavMesh
    private UnityEngine.AI.NavMeshAgent agent;
    public Transform newDest;

    // Raycast Direction Initializer
    Vector3 north;
    Vector3 south;
    Vector3 east;
    Vector3 west;

    // Start is called before the first frame update
    void Start()
    {
        playerCollider = GetComponent<CapsuleCollider>();
        playerAnim = GetComponent<Animator>();
        AddEvent(1, 2f, "AfterPlayerDeath", 0);
        playerDone = GameObject.Find("LevelFinishUI");
        playerDeath = GameObject.Find("GameOverUI");

        // Initialize all the enemies in scene
        if (enemies == null) {
            enemies = GameObject.FindGameObjectsWithTag("Enemy");
        }

        // NavMesh
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();

        // Initialize rayCast directions towards player orientation
        north = transform.forward;
        south = -transform.forward;
        east = transform.right;
        west = -transform.right;
    }

    // Update is called once per frame
    void Update()
    {
        rayForward = new Ray(transform.position + new Vector3(0f, playerCollider.center.y, 0f), north);
        rayBackward = new Ray(transform.position + new Vector3(0f, playerCollider.center.y, 0f), south);
        rayRight = new Ray(transform.position + new Vector3(0f, playerCollider.center.y, 0f), east);
        rayLeft = new Ray(transform.position + new Vector3(0f, playerCollider.center.y, 0f), west);


        Debug.DrawRay(rayForward.origin, rayForward.direction*rayDistance, Color.red);
        Debug.DrawRay(rayBackward.origin, rayBackward.direction * rayDistance, Color.green);
        Debug.DrawRay(rayLeft.origin, rayLeft.direction * rayDistance, Color.yellow);
        Debug.DrawRay(rayRight.origin, rayRight.direction * rayDistance, Color.blue);

        if (Input.GetButtonDown("Vertical"))
        {
            // int moveVertical;
            if(Input.GetAxis("Vertical") > 0)
            {
                if (Physics.Raycast(rayForward, out hit) && (hit.collider.tag == "PlayerPath" || hit.collider.tag == "Enemy") && !agent.pathPending && agent.remainingDistance == 0)
                {
                    if (hit.distance < rayDistance)
                    {
                        // moveVertical = -1 + ((int)Input.GetAxis("Vertical") * 0);
                        // Vector3 movement = new Vector3(0f, 0f, moveVertical);
                        // transform.Translate(movement * moveSpeed);

                        newDest = hit.transform.parent.gameObject.transform;

                        // print("RayForward");
                        // print(hit.transform.parent.gameObject.transform);

                        if (!agent.pathPending) {
                            agent.destination = newDest.position;
                        }


                        if (hit.collider.tag == "PlayerPath")
                        {
                            GetComponent<AudioSource>().PlayOneShot(moveSound);
                            detectAdvancedEnemy();
                        }
                        


                        // Trigger enemy movement
                        moveEnemies();
                    }
                }
                
            } else
            {
                if (Physics.Raycast(rayBackward, out hit) && (hit.collider.tag == "PlayerPath" || hit.collider.tag == "Enemy") && !agent.pathPending && agent.remainingDistance == 0)
                {
                    if (hit.distance < rayDistance)
                    {
                        // moveVertical = 1 + ((int)Input.GetAxis("Vertical") * 0);
                        // Vector3 movement = new Vector3(0f, 0f, moveVertical);
                        // transform.Translate(movement * moveSpeed);

                        newDest = hit.transform.parent.gameObject.transform;

                        // print("RayBackward");
                        // print(hit.transform.parent.gameObject.transform);

                        if (!agent.pathPending) {
                            agent.destination = newDest.position;
                        }

                        if (hit.collider.tag == "PlayerPath")
                        {
                            GetComponent<AudioSource>().PlayOneShot(moveSound);
                            detectAdvancedEnemy();
                        }
                        


                        // Trigger enemy movement
                        moveEnemies();
                    }
                }
            }
            
        } else if (Input.GetButtonDown("Horizontal"))
        {
            // int moveHorizontal;
            if (Input.GetAxis("Horizontal") > 0)
            {
                if (Physics.Raycast(rayRight, out hit) && (hit.collider.tag == "PlayerPath" || hit.collider.tag == "Enemy") && !agent.pathPending && agent.remainingDistance == 0)
                {
                    
                    if (hit.distance < rayDistance)
                    {
                        // moveHorizontal = -1 + ((int)Input.GetAxis("Horizontal") * 0);
                        // Vector3 movement = new Vector3(moveHorizontal, 0f, 0f);
                        // transform.Translate(movement * moveSpeed);

                        newDest = hit.transform.parent.gameObject.transform;

                        // print("RayRight");
                        // print(hit.transform.parent.gameObject.transform);

                        if (!agent.pathPending) {
                            agent.destination = newDest.position;
                        }

                        if (hit.collider.tag == "PlayerPath")
                        {
                            GetComponent<AudioSource>().PlayOneShot(moveSound);
                            detectAdvancedEnemy();
                        } 

                        
                        // Trigger enemy movement
                        moveEnemies();
                    }
                }
                
            }
            else
            {
                if (Physics.Raycast(rayLeft, out hit) && (hit.collider.tag == "PlayerPath" || hit.collider.tag == "Enemy") && !agent.pathPending && agent.remainingDistance == 0)
                {
                    if (hit.distance < rayDistance)
                    {
                        // moveHorizontal = 1 + ((int)Input.GetAxis("Horizontal") * 0);
                        // Vector3 movement = new Vector3(moveHorizontal, 0f, 0f);
                        // transform.Translate(movement * moveSpeed);
                        
                        newDest = hit.transform.parent.gameObject.transform;

                        // print("RayLeft");
                        // print(hit.transform.parent.gameObject.transform);

                        if (!agent.pathPending) {
                            agent.destination = newDest.position;
                        }

                        if (hit.collider.tag == "PlayerPath")
                        {
                            GetComponent<AudioSource>().PlayOneShot(moveSound);
                            detectAdvancedEnemy();
                        }
                        


                        // Trigger enemy movement
                        moveEnemies();
                    }
                }
                
            }
            
         
        }

        // Set walking speed
        playerAnim.SetFloat("Speed", agent.velocity.sqrMagnitude);

        // if (Input.GetButtonDown("Jump")) {
        //     foreach (GameObject enemy in enemies) {

        //         BaseEnemyMovement changeEnemy = (BaseEnemyMovement) enemy.GetComponent(typeof(BaseEnemyMovement));

        //         changeEnemy.setAdvancedMovement(newPoint);

        //     }

        // }
        
    }

    private void detectAdvancedEnemy()
    {
        // Detect if Enemy within Player range
        AdvancedDetection detect = (AdvancedDetection) GetComponent(typeof(AdvancedDetection));
        detect.AdvancedAIDetection();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "ExitPath")
        {
            GetComponent<AudioSource>().PlayOneShot(levelComplete);
            LevelCompleteController levelFinish = playerDone.GetComponentInChildren<LevelCompleteController>();
            levelFinish.LevelFinish();
        }

        if (other.gameObject.CompareTag("Enemy"))
        {
           GetComponent<AudioSource>().PlayOneShot(eliminateSound);
        }
    }
    // Script to trigger enemy movement
    public void moveEnemies() {

        foreach (GameObject enemy in enemies) {

            BaseEnemyMovement moveEnemy = (BaseEnemyMovement) enemy.GetComponent(typeof(BaseEnemyMovement));
            moveEnemy.move();

            // AdvancedEnemyMovement moveAdvancedEnemy = (AdvancedEnemyMovement) enemy.GetComponent(typeof (AdvancedEnemyMovement));
            // moveAdvancedEnemy.move();

        }
    }

    public void PlayerDeath()
    {
        //playerAnim.applyRootMotion = false;
        GetComponent<AudioSource>().PlayOneShot(deathSound);
        playerAnim.SetBool("playerDeath", true);

    }

    void AddEvent(int Clip, float time, string functionName, float floatParameter)
    {
        //playerAnim = GetComponent<Animator>();
        AnimationEvent animationEvent = new AnimationEvent();
        animationEvent.functionName = functionName;
        animationEvent.floatParameter = floatParameter;
        animationEvent.time = time;
        AnimationClip clip = playerAnim.runtimeAnimatorController.animationClips[Clip];
        clip.AddEvent(animationEvent);
    }

    private void AfterPlayerDeath()
    {
        GameOverController gameOverInfo = playerDeath.GetComponentInChildren<GameOverController>();
        gameOverInfo.PlayerDeathActive();
        print("player death");
    }

    




}
