﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdvancedDetection : MonoBehaviour
{
    // Keeps list of alerted Advanced AI's
    private List<GameObject> alertEnemy;
    private GameObject[] currEnemies; // Keeps list of current enemies
    private float coolDown;
    private float coolRate = 20.0f;


    //Player Detection radius
    private float radius = 11;

    // Start is called before the first frame update
    void Start()
    {
        alertEnemy = new List<GameObject>(); 
        currEnemies = GameObject.FindGameObjectsWithTag("Enemy");
        coolDown = 0.0f;
    }

    public void AdvancedAIDetection()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius, 1);
        //Debug.Log("Position X: " + transform.position.x + " Position Y: " + transform.position.x + "Position Z: " + transform.position.x);
        foreach(GameObject enemy in currEnemies)
        {
            foreach (Collider hitCollider in hitColliders)
            {
                if(hitCollider.gameObject.CompareTag("Enemy") && enemy.name == "AdvancedGuard" && enemy.transform.position == hitCollider.gameObject.transform.position && Time.time > coolDown)
                {
                    coolDown = Time.time + coolRate;
                    Debug.Log("PLAYER IN RANGE OF AI");
                    BaseEnemyMovement alertEnemy = (BaseEnemyMovement) enemy.gameObject.GetComponent(typeof(BaseEnemyMovement));
                    alertEnemy.setAdvancedMovement(transform); // Set advanced movement of detected AI to player position
                }
            }
        }
    }
}
