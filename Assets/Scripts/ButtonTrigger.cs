﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
    How to use ButtonTrigger script:
    1.) Have Empty GameObject with animators attached that effect the child object 
        a) Gates Animator alters the Left and Right door rotation), so you need the base GameObject
        b) For levers, only the lever itself is animated, so you need to drag the lever component, not the base GameObject, into the inspector.
    2.) Attach this script to moveSpace (PlayerPath CapsuleCollider) where Player can trigger event, set capsule collider to trigger
    3.) Size depends on how many objects with anims you have (for ex. 2 moving gates + 2 lever switches = Size of 4) Drag relevant GameObjects with anims into "Object Anims" in the inspector
    4.) Make sure Player has a rigid body with constraints on position and rotation.
*/
public class ButtonTrigger : MonoBehaviour
{
    private bool isPressed;
    private float coolDown;
    private float coolRate = 3.0f;
    public GameObject[] objectAnims;
    public GameObject[] gateRestrictions;
    public AudioClip gateOpen;
    public AudioClip gateShut;
    private Animator anim;

    void Start()
    {
        isPressed = false;
        gateRestrictions = GameObject.FindGameObjectsWithTag("GateRestriction");
    }

    // Update is called once per frame
    void OnTriggerStay(Collider collide)
    {
        if (collide.gameObject.CompareTag("Player") && Input.GetKey("space") && Time.time > coolDown)
        {
            coolDown = Time.time + coolRate;
            isPressed = !isPressed;
            
            foreach(GameObject objectAnim in objectAnims)
            {
                anim = objectAnim.GetComponent<Animator>();
                anim.SetBool("ButtonPress", isPressed);
                // Carve/uncarve gate
            }
            foreach(GameObject gate in gateRestrictions)
            {
                gate.SetActive(!isPressed);
                if(gate.activeSelf)
                {
                    Debug.Log(gateShut);
                    GetComponent<AudioSource>().PlayOneShot(gateShut);
                }
                else if (!gate.activeSelf)
                {
                    Debug.Log(gateOpen);
                    GetComponent<AudioSource>().PlayOneShot(gateOpen);
                }
            }
        }
    }
}
