﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdvancedEnemyMovement : MonoBehaviour
{
    //Animator/Movement
    public Animator enemyAnimator;

    //Ai Nav
    public UnityEngine.AI.NavMeshAgent enemyAgent;
    public Transform finalPoint;
    public Transform currentPoint;
    public Transform previousPoint;
    // private int destPoint = 0;

    //Raycast
    private float rayDistance = 7.5f;
    private Ray rayForward;
    // private Ray rayLeft;
    // private Ray rayRight;
    // private Ray rayBackward;
    private RaycastHit hit;

    public bool advancedTarget;

    //Collider
    private CapsuleCollider enemyCollider;

    // Start is called before the first frame update
    void Start()
    {
        enemyAnimator = GetComponent<Animator>();
        enemyAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        enemyCollider = GetComponent<CapsuleCollider>();


        // Implement setting and calling of final destination once distraction and player radius entered.
        currentPoint = finalPoint;

    }

    // Update is called once per frame
    void Update()
    {

        rayForward = new Ray(transform.position + new Vector3(0f, enemyCollider.center.y, 0f), transform.forward);

        // rayLeft = new Ray(transform.position + new Vector3(0f, enemyCollider.center.y, 0f), -transform.right);

        // rayRight = new Ray(transform.position + new Vector3(0f, enemyCollider.center.y, 0f), transform.right);



        Debug.DrawRay(rayForward.origin, rayForward.direction*rayDistance, Color.red);

        // Debug.DrawRay(rayLeft.origin, rayLeft.direction*rayDistance, Color.blue);

        // Debug.DrawRay(rayRight.origin, rayRight.direction*rayDistance, Color.yellow);

        // Debug.DrawRay(rayRight.origin, rayRight.direction*rayDistance, Color.green);

        //Testing
        // if (enemyAgent.velocity.sqrMagnitude > 0.1) {
        //     print(enemyAgent.velocity.sqrMagnitude);
        // }

        if (enemyAgent.remainingDistance < 0.5f) {
            previousPoint = currentPoint;
        }

        enemyAnimator.SetFloat("Speed", enemyAgent.velocity.sqrMagnitude);



    }

    public void move() {
        advancedTarget = true;

        if (advancedTarget) {

            currentPoint = finalPoint;

            Physics.Raycast(rayForward, out hit);

            if (!enemyAgent.pathPending) {
                // Vector3 forceForward = new Vector3(10f, 0f, 0f);
                // enemyAgent.Move(forceForward);
                enemyAgent.destination = currentPoint.position;
                
            }
        }

    }


    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("TargetReset")) {
            // print("Collision Occurred");

            //Example with player and enemy tag
            // if (Physics.Raycast(rayForward, out hit) && (hit.collider.tag == "PlayerPath" || hit.collider.tag == "Enemy" || hit.collider.tag == "Player")) {

            if (Physics.Raycast(rayForward, out hit) && (hit.collider.tag == "PlayerPath")) {
                // print("Found new destination");
                currentPoint = hit.transform;

                if (!enemyAgent.pathPending) {
                    enemyAgent.destination = currentPoint.position;
                }

                // print("hit.collider.gameObject.name");
                // enemyAnimator.SetFloat("Speed", enemyAgent.velocity.sqrMagnitude);
            }

        }

        if (other.gameObject.CompareTag("Doors")) {
            currentPoint = previousPoint;

            if (!enemyAgent.pathPending) {
                enemyAgent.destination = currentPoint.position;
            }

        }

    }

    private void setAdvancedMovement(Transform newPoint) {
        finalPoint = newPoint.transform;

        // advancedMovement = true;
    }

}
