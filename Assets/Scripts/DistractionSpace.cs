﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Resource: https://forum.unity.com/threads/3d-character-picking-up-item.900098/ for making sure I could get player to "pick up" object
public class DistractionSpace : MonoBehaviour
{
    public GameObject distractionObject;
    //public GameObject limitMovement;
    public GameObject[] selectablePositions;
    public GameObject myHands; //reference to your hands/the position where you want your object to go
    public float radius;
    private bool hasItem = false; // a bool to see if you have an item in your hand
    private bool isThrown = false;
    private List<GameObject> alertEnemy;
    private GameObject[] currEnemies;
    private float coolDown;
    private float coolRate = 2.5f;

    // Start is called before the first frame update
    void Start()
    {   
        alertEnemy = new List<GameObject>(); 
        currEnemies = GameObject.FindGameObjectsWithTag("Enemy");
        //limitMovement.SetActive(false);
    }

    private void OnTriggerStay(Collider other) // to see when the player enters the collider
    {
        if(other.gameObject.tag == "Player") //on the object you want to pick up set the tag to be anything, in this case "object"
        {
            // Pause player movement 
            RaycastHit hit;
            if (Input.GetKeyDown("space") && hasItem == false && isThrown == false)  // Put object in Player hands
            {
                distractionObject.transform.position = myHands.transform.position; // sets the position of the object to your hand position
                distractionObject.transform.parent = myHands.transform; //makes the object become a child of the parent so that it moves with the hands
                hasItem = true;
                //limitMovement.SetActive(hasItem);
            } 
            else if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100) && Input.GetMouseButton(0) && hit.transform.tag == "PlayerPath" && hasItem == true && isThrown == false && Time.time > coolDown) // If hit is in selectablePositions then....
            {
                coolDown = Time.time + coolRate;
                Vector3 destinationPos = hit.transform.position; // Check valid destination
                
                if(SelectablePosition(destinationPos) == true) // Check if position is in list of selectable
                {
                    Transform destination = hit.transform; // Agent destination
                    distractionObject.transform.parent = null; // make the object no be a child of the hand

                    ThrowToPoint(destination);
                    // Call function to see if enemy is in range of distraction thrown --> Trigger to start moving (destination, range) 
                        // If raycast hits moveSpace, continue along path, else, rotate and try again. If hit destination return to normal movement

                    hasItem = false;
                    isThrown = true;
                    //limitMovement.SetActive(hasItem);
                }
            }
        }
    }

    private void ThrowToPoint(Transform point)
    {
        distractionObject.transform.LookAt(point);

        if((-0.04 <= distractionObject.transform.rotation.y && distractionObject.transform.rotation.y <= 0.04) || (0.98 <= distractionObject.transform.rotation.y && distractionObject.transform.rotation.y <= 1) || (-0.72 <= distractionObject.transform.rotation.y && distractionObject.transform.rotation.y <= -0.68))
        {
            distractionObject.GetComponent<Animator>().SetTrigger("Straight");
        }
        else
        {
            distractionObject.GetComponent<Animator>().SetTrigger("Angled");   
        }
        
        Collider[] hitColliders = Physics.OverlapSphere(point.position, radius, 1);
        foreach(GameObject enemy in currEnemies)
        {
            foreach (Collider hitCollider in hitColliders)
            {
                if(hitCollider.gameObject.transform.position == enemy.transform.position)
                {
                    alertEnemy.Add(enemy.gameObject);
                    Debug.Log(enemy.gameObject.name);
                }
            }
        }
        AlertEnemies(alertEnemy, point);
    }

    private void AlertEnemies(List<GameObject> enemies, Transform dest)
    {
        // How to access all enemies in alertEnemy array
        foreach(GameObject enemy in enemies)
        {
            BaseEnemyMovement alertEnemy = (BaseEnemyMovement) enemy.GetComponent(typeof(BaseEnemyMovement));
            alertEnemy.setAdvancedMovement(dest);
            Debug.Log("Name: " + enemy.gameObject.name);
            Debug.Log("X: " + dest.position.x);
            Debug.Log("Y: " + dest.position.y);
            Debug.Log("Z: " + dest.position.z);
        }
    }

    private bool SelectablePosition(Vector3 destination)
    {
        bool selectable = false;
        float yPos = 1;
        Vector3 upDestination = new Vector3(destination.x, yPos, destination.z);

        foreach(GameObject pos in selectablePositions)
        {
            if((destination.x == pos.transform.position.x) && (destination.z == pos.transform.position.z))
            {
                selectable = true;
            }
        }
        return selectable;
    }
}
