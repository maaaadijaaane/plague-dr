﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BaseEnemyMovement : MonoBehaviour
{

    public Transform deadSpawn;
    private Animator guardAnim;
    private CapsuleCollider enemyCollider;
    private bool playerAttack = false;
    private bool navigating = false;
    private bool attacked = false;
    private GameObject currentPlayer;

    // Nav AI
    private NavMeshAgent agent;
    private int destPoint = 0;
    private int previousDest = 0;

    public Transform finalPoint;
    public Transform currentPoint;
    public Transform previousPoint;

    // Raycast
    private float rayDistance = 10f;
    private Ray rayForward;
    private RaycastHit hit;

    // Base Movement
    public Transform[] moveSpaces;

    // Advanced Movement
    public bool advancedMovement;
    private bool retargeting;
    
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        enemyCollider = GetComponent<CapsuleCollider>();
        guardAnim = GetComponent<Animator>();
        playerAttack = false;


        // advancedMovement == false;
    }

    // Update is called once per frame
    void Update()
    {
        //Checking constantly if player get within attack sight
        rayForward = new Ray(transform.position + new Vector3(0f, enemyCollider.center.y, 0f), transform.forward);

        if (Physics.Raycast(rayForward, out hit, 10) && (hit.collider.tag == "Player"))
        {
            
            //Check if player haven't attack and if it not attack yet
            if(!playerAttack && !(attacked))
            {
                attacked = true;
                AttackPlayer();
            }

        }
        
        // Debug
        Debug.DrawRay(rayForward.origin, rayForward.direction * rayDistance, Color.red);

        // Set Animation Speed and Previous Position
        if (retargeting) {
            if (Physics.Raycast(rayForward, out hit, 3) && (hit.collider.tag == "PlayerPath")) {
                
                // print("Found new destination");
                currentPoint = hit.transform.parent.gameObject.transform;

                if (!agent.pathPending) {
                    agent.destination = currentPoint.position;
                }

                // print(hit.transform.parent.gameObject.name);
                // enemyAnimator.SetFloat("Speed", agent.velocity.sqrMagnitude);
                retargeting = false;

            }

        }

        if (!agent.pathPending && agent.remainingDistance < 0.5f) {
            previousPoint = currentPoint.transform;
        }

        guardAnim.SetFloat("Speed", agent.velocity.sqrMagnitude);

    }


    private void AttackPlayer()
    {
        currentPlayer = GameObject.FindGameObjectWithTag("Player");
        guardAnim.enabled = false;
        transform.position = currentPlayer.transform.position;
        PlayerMoveset playerDead = (PlayerMoveset)currentPlayer.GetComponent(typeof(PlayerMoveset));
        playerDead.PlayerDeath();
    }

    public void move() {

        // Advanced/Pathing Movement
        if (advancedMovement) {
            // print("Advanced Move");

            currentPoint = finalPoint.transform;

            Physics.Raycast(rayForward, out hit);

            if (!agent.pathPending) {
                // Vector3 forceForward = new Vector3(10f, 0f, 0f);
                // agent.Move(forceForward);
                agent.destination = currentPoint.position;
                
            }
        }

        // Base movement
        else {

            // print("Base Move");
            rayForward = new Ray(transform.position + new Vector3(0f, enemyCollider.center.y, 0f), transform.forward);
            if (Physics.Raycast(rayForward, out hit) && (hit.collider.tag != "Doors")) {
                if (moveSpaces.Length > 0) {

                    // transform.position = new Vector3(moveSpaces[destPoint].position.x, transform.position.y, moveSpaces[destPoint].position.z);

                    currentPoint = moveSpaces[destPoint].transform;

                    if (!agent.pathPending) {
                        agent.destination = currentPoint.position;
                    }

                    destPoint = (destPoint + 1) % moveSpaces.Length;
                    
                    // transform.LookAt(new Vector3(moveSpaces[destPoint].position.x, transform.position.y, moveSpaces[destPoint].position.z));
                }

            }
            else if(((Physics.Raycast(rayForward, out hit) && hit.collider.tag == "Doors") || !(Physics.Raycast(rayForward, out hit))) && moveSpaces.Length > 0)
            {
                currentPoint = moveSpaces[destPoint].transform;

                if (!agent.pathPending) {
                    agent.destination = currentPoint.position;
                }
                destPoint = (destPoint + 1) % moveSpaces.Length;
            }
            // else {


                // I don't know why this is here -RJ
            
                // if (moveSpaces.Length != 0)
                // {
                //     destPoint = (destPoint + 1) % moveSpaces.Length;

                //     transform.LookAt(new Vector3(moveSpaces[destPoint].position.x, transform.position.y, moveSpaces[destPoint].position.z));
                // }
                //destPoint = (destPoint + 1) % moveSpaces.Length;
                //transform.LookAt(new Vector3(moveSpaces[destPoint].position.x, transform.position.y, moveSpaces[destPoint].position.z));

            // }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playerAttack = true;
            Death();
        }

        if (other.gameObject.CompareTag("TargetReset")) {
            // print("Collision Occurred");

            //Example with player and enemy tag
            // if (Physics.Raycast(rayForward, out hit) && (hit.collider.tag == "PlayerPath" || hit.collider.tag == "Enemy" || hit.collider.tag == "Player")) {

            retargeting = true;
        }

        if (other.gameObject.CompareTag("Doors")) {

            // currentPoint = previousPoint.transform;
            if (!agent.pathPending) {
                agent.destination = previousPoint.position;
            }

        }

    }

    private void Death()
    {
        GetComponent<NavMeshAgent>().enabled = false;
        if (playerAttack)
        {
            guardAnim.SetBool("playerAttack", true);
        }
    }

    private void AfterDeath()
    {
        guardAnim.applyRootMotion = true;
        guardAnim.enabled = false;
        transform.position = deadSpawn.transform.position;
        //print("Guard should move to the deadspawn");
        
    }

    public void setAdvancedMovement(Transform newPoint) {
        finalPoint = newPoint.transform;

        advancedMovement = true;
    }

}
