﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelCompleteController : MonoBehaviour
{
    public GameObject levelCompleteScreen;

    void Awake()
    {
        levelCompleteScreen.SetActive(false);
    }

    public void LevelFinish()
    {
        levelCompleteScreen.SetActive(true);
        Time.timeScale = 0;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
