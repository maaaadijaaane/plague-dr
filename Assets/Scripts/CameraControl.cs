﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public Transform pos1;
    public Transform posLook;
    private float moveSpeed = 12f;
    private float rotationSpeed = 1f;
    private Quaternion lookRotation;
    private Vector3 direction;
    private bool enable = false;
    private bool revert = false;
    private Vector3 oldPos;
    private Quaternion oldLook;

    private void Start()
    {
        oldPos = transform.position;
        oldLook = transform.rotation;
    }


    private void Update()
    {
        if (enable && !(revert)) 
        {
            transform.position = Vector3.MoveTowards(transform.position, pos1.transform.position, moveSpeed * Time.deltaTime);

            direction = (posLook.transform.position - transform.position).normalized;
            lookRotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * rotationSpeed);
        }

        if(revert)
        {
            transform.position = Vector3.MoveTowards(transform.position, oldPos, moveSpeed * Time.deltaTime);
            transform.rotation = Quaternion.Slerp(transform.rotation, oldLook, Time.deltaTime * rotationSpeed);
        }
    }

    public void CameraMove()
    {
        enable = true;
        revert = false;
    }

    public void CameraRevert()
    {
        enable = false;
        revert = true;
    }
}
