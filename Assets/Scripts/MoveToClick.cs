﻿using System.Collections;
using System.Collections.Generic;
    // MoveToClickPoint.cs
    using UnityEngine;
    using UnityEngine.AI;
    
public class MoveToClick : MonoBehaviour {
    public GameObject[] points;
    NavMeshAgent agent;
    
    void Start() {
        agent = GetComponent<NavMeshAgent>();
        points = GameObject.FindGameObjectsWithTag("PlayerPath");
    }
    
    void Update() {
        if (Input.GetMouseButtonDown(0)) {
            RaycastHit hit;
            
            if ((Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100) && hit.transform.tag == "PlayerPath")) {
                agent.destination = hit.point;
            }
        }
    }
}