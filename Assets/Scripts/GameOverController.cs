﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverController : MonoBehaviour
{
    public GameObject gameOverScreen;
    // Start is called before the first frame update

    void Awake()
    {
        gameOverScreen.SetActive(false);
    }

    public void PlayerDeathActive()
    {
        gameOverScreen.SetActive(true);
        Time.timeScale = 0;
    }

    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
