﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseController : MonoBehaviour
{
    public GameObject pauseScreen;
    private float pausedTimeScale;
    private bool isPaused = false;

    void Awake()
    {
        pauseScreen.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(!isPaused && Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
        }
        else if(isPaused && Input.GetKeyDown(KeyCode.Escape))
        {
            Unpause();
        }
    }

    private void Pause()
    {
        isPaused = true;
        pauseScreen.SetActive(true);
        pausedTimeScale = Time.timeScale;
        Time.timeScale = 0;
    }

    public void Unpause()
    {
        if(isPaused)
        {
            isPaused = false;
            pauseScreen.SetActive(false);
            Time.timeScale = pausedTimeScale;
        }
    }
}
